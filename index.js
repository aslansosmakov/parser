const path = require('path');
const cheerio = require('cheerio');
const XLSX = require('xlsx');
const chunk = require('lodash/chunk');
const axios = require('axios');
const download = require('image-downloader');
const {v4: uuid} = require('uuid');

const DOMAIN = 'https://www.aqwella.com';
const PAGE_URL = `${DOMAIN}/collection/mobi/`;

async function run() {
    const {data} = await axios(PAGE_URL);

    const $pageHtml = cheerio.load(data);
    const goods = [];
    $pageHtml('.catalog .catalog__wrap .catalog__item-img a').each((idx, item) => {
        goods.push($pageHtml(item).attr('href'));
    });
    
    const result = goods.map(async (item) => {
        const { data } = await axios.get(DOMAIN + item);
        const $ = cheerio.load(data);

        const name = $('.prod-detail__info .detail-title').text();
        const model = $('.prod-detail__info .detail-artikul').text().split(': ')[1];
        const description = $('.prod-detail__info .detail-name').text();
        let ttx = [];
        $('.params-box .param-column div').each((idx, item) => {
            if (!$(item).hasClass('gor-line')) {
                $(item).contents().each((i, c) => {
                    if ($(c).text() != '') {
                        ttx.push($(c).text());
                    }
                });
            }
        });

        ttx = chunk(ttx, 2);

        let images = [];

        $('.prod-detail__slider-mini > .slick-slide img').each((idx, item) => {
            images.push($(item).attr('src'));
        });

        const imagePromises = [];
        const imagesNames = [];
        images.forEach((img) => {
            const imageName = uuid() + path.extname(img);
            imagesNames.push('/img/' + imageName);

            imagePromises.push(download.image({
                url: `${DOMAIN}${img}`,
                dest: './img/' + imageName,
            }));
        });

        await Promise.all(imagePromises);

        const bb = {
            name,
            model,
            description,
            images: imagesNames.join(',')
        };

        ttx.forEach(item => {
            bb[item[0]] = item[1];
        });

        return bb;
    });

    const products = await Promise.all(result);
    



    const workSheet = XLSX.utils.json_to_sheet(products);
    const workBook = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(workBook, workSheet, "products");
    XLSX.write(workBook, { bookType: 'xlsx', type: "buffer" });
    XLSX.write(workBook, { bookType: "xlsx", type: "binary" });
    XLSX.writeFile(workBook, "products.xlsx");
}

run().catch(console.error).finally(() => process.exit(0));